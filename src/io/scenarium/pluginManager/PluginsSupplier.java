/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.pluginManager;

// Rename PluginsCoreSupplier
public interface PluginsSupplier {
	// TODO maybe add the input argument list
	// TODO return list of supplier available.
	/** Initialize the plug-in internal properties. */
	default void birth() {}

	/** Load the plug-in property in the local plug-in supplier system
	 * @param pluginInterface Object that CAN implement the interface of the sub plug-in */
	default void loadPlugin(Object pluginInterface) {}

	/** Re-load the plug-in property in the local plug-in supplier system
	 * @param pluginInterfaceOld Object that CAN implement the interface of the sub plug-in (reference on the old plug-in of this module)
	 * @param pluginInterfaceNew Object that CAN implement the interface of the sub plug-in (new module manager) */
	default void reloadPlugin(Object pluginInterfaceOld, Object pluginInterfaceNew) {
		loadPlugin(pluginInterfaceNew);
	}

	/** Un-Load the plug-in in the local plug-in supplier system
	 * @param pluginInterface Object that CAN implement the interface of the sub plug-in */
	default void unloadPlugin(Object pluginInterface) {}

	/** Last call before close the plug-in internal properties. */
	default void death() {}

	// TODO show if it is needed to keep this here... solution:
	// * have a module manager that manage the adding and the remove of module (JARS) and a plug-in manager that depend on the module manager.
	// with this beanmanager will register on moduleManager with the intermediate of the application.
	// * Force to have a single module for one pluginSuplier
	// * show this later.

	/** A new module has been register in the plug-in system.
	 * @param module Module interface */
	default void registerModule(Module module) {}

	/** A module MUST be unregister. You need to purge all your reference on it
	 * @param module Module interface */
	default void unregisterModule(Module module) {}

}