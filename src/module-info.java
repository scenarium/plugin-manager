import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.pluginmanager {
	uses PluginsSupplier;

	exports io.scenarium.pluginManager;

	requires transitive io.scenarium.logger;
}
