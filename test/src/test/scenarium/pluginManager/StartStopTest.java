/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package test.scenarium.pluginManager;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import io.scenarium.pluginManager.ModuleManager;
import io.scenarium.pluginManager.internal.Log;

import org.junit.Test;

public class StartStopTest {
	private static final Random RAND = new Random();

	private String randomString(int count) {
		String s = new String();
		int nbChar = count;
		for (int i = 0; i < nbChar; i++) {
			char c = (char) RAND.nextInt();
			while ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9'))
				c = (char) RAND.nextInt();
			s = s + c;
		}
		return s;
	}

	private Path getTemporaryFolder() {
		String strTmp = System.getProperty("java.io.tmpdir");
		return Paths.get(strTmp + FileSystems.getDefault().getSeparator() + "scTest" + FileSystems.getDefault().getSeparator() + randomString(25));
	}

	private void createFolder(Path path) throws IOException {
		if (!Files.exists(path)) {
			Log.info("Create folder: " + path);
			Files.createDirectories(path);
		}
	}

	@SuppressWarnings("unused")
	private void removeFolder(Path path) throws IOException {
		if (Files.exists(path)) {
			Log.info("Delete folder: " + path);
			if (false)
				// TODO remove right now ... not sure...
				Files.delete(path);
		}
	}

	@Test
	public void testType() throws IOException {
		Log.info("Start TEST 1");
		Path testDirectory = getTemporaryFolder();
		createFolder(testDirectory);
		ModuleManager.birth(testDirectory.toString());
		ModuleManager.death();
		removeFolder(testDirectory);
		Log.info("END TEST 1");
	}

	@Test
	public void displayListOfModules() throws IOException {
		Log.info("Start TEST 2");
		Path testDirectory = getTemporaryFolder();
		createFolder(testDirectory);
		ModuleManager.birth(testDirectory.toString());
		Log.print("list of modules " + ModuleManager.PARENT_MODULES_NAME);
		ModuleManager.death();
		removeFolder(testDirectory);
		Log.info("END TEST 2");
	}

}
